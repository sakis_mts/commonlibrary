package com.sonin.soninandroidcommonlib

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.sonin.soninandroidcommon.BuildConfig
import com.sonin.soninandroidcommon.Retrofit.RetrofitActivity
import com.sonin.soninandroidcommonlib.accountManager.AccountAuthenticator


/**
 * Created by athanasios.moutsioul on 23/08/2017.
 */
internal class AuthenticatorService : Service() {
    override fun onBind(intent: Intent): IBinder {

        val authenticator = AccountAuthenticator(this, RetrofitActivity::class.java, BuildConfig.ENDPOINT)

        return authenticator.getIBinder()
    }
}