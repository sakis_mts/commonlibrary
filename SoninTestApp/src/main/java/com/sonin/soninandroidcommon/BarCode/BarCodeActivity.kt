package com.sonin.soninandroidcommon.BarCode

import com.google.android.gms.vision.barcode.Barcode
import com.sonin.soninandroidcommonlib.BarCodeManager.BarcodeCaptureActivity

/**
 * Created by athanasios.moutsioul on 11/10/2017.
 */
class BarCodeActivity: BarcodeCaptureActivity() {
    override fun onBarcodeDetected(barcode: Barcode) {
        super.onBarcodeDetected(barcode)

        println("The barcode changed: ${barcode.displayValue}")
    }
}