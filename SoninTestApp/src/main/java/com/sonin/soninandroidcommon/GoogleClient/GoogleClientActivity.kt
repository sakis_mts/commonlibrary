package com.sonin.soninandroidcommon.GoogleClient

import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sonin.soninandroidcommon.R
import com.sonin.soninandroidcommonlib.activities.BaseActivityWithGoogleClient
import com.sonin.soninandroidcommonlib.utils.SwipeDetector
import kotlinx.android.synthetic.main.activity_google_client.*

class GoogleClientActivity : BaseActivityWithGoogleClient() , SwipeDetector.onSwipeEvent{
    override fun SwipeEventDetected(v: View?, SwipeType: SwipeDetector.SwipeTypeEnum?) {

        println(SwipeType)
    }

    override fun onLocationChanged(location: Location) {
        super.onLocationChanged(location)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_client)

        changeLocationRequestFrequency(10000,5000)

        val swipe = SwipeDetector(tvWelcome, SwipeDetector.ON_FINISH_DETECTION)
        swipe.setOnSwipeListener(this)

    }
}
