package com.sonin.soninandroidcommon

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sonin.soninandroidcommonlib.BarCodeManager.BarcodeCaptureActivity
import com.sonin.soninandroidcommon.GoogleClient.GoogleClientActivity
import com.sonin.soninandroidcommon.Retrofit.RetrofitActivity
import kotlinx.android.synthetic.main.activity_navigation.*
import org.jetbrains.anko.intentFor
import android.content.Intent
import com.sonin.soninandroidcommon.phoneView.PhoneViewActivity


class NavigationActivity : AppCompatActivity() , View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        tvRecyclerView.setOnClickListener(this)
        tvRetrofit.setOnClickListener(this)
        tvGoogleClient.setOnClickListener(this)
        tvBarCode.setOnClickListener(this)
        tvPhoneView.setOnClickListener(this)


    }

    override fun onClick(view: View?) {

        view.let {

            when(it?.id){

                 R.id.tvRecyclerView->{ startActivity(intentFor<RecyclerViewWithActionModeActivity>())}
                R.id.tvRetrofit->{ startActivity(intentFor<RetrofitActivity>())}
                R.id.tvGoogleClient->{ startActivity(intentFor<GoogleClientActivity>())}
                R.id.tvPhoneView->{ startActivity(intentFor<PhoneViewActivity>())}
                R.id.tvBarCode->{
                    val intent = Intent(this, BarcodeCaptureActivity::class.java)
                    intent.putExtra(BarcodeCaptureActivity.AutoFocus,true)
                    intent.putExtra(BarcodeCaptureActivity.UseFlash, false)

                    startActivity(intent)}

            }


        }
    }


}
