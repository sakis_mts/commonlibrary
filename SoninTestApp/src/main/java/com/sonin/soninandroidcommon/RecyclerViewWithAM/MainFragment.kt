package com.sonin.soninandroidcommon

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sonin.soninandroidcommon.RecyclerViewWithAM.TestAdapter
import com.sonin.soninandroidcommonlib.recyclerView.FragmentRecyclerVIewWithActionMode
import com.sonin.soninandroidcommonlib.recyclerView.Toolbar_ActionMode_Callback
import com.sonin.soninandroidcommonlib.recyclerView.Toolbar_ActionMode_Callback.onActionButtonCLicked
import com.sonin.soninandroidcommonlib.utils.TangerineLog
import kotlinx.android.synthetic.main.main_fragment.*
import com.sonin.soninandroidcommonlib.recyclerView.RecyclerClick_Listener
import com.sonin.soninandroidcommonlib.recyclerView.RecyclerTouchListener




/**
 * Created by athanasios.moutsioul on 18/09/2017.
 */
class MainFragment: FragmentRecyclerVIewWithActionMode(), onActionButtonCLicked {


    lateinit var adapter: TestAdapter
    lateinit var arrMyModel: ArrayList<MyModel>

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.main_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initVars()
    }

    private fun initVars() {

        arrMyModel = ArrayList<MyModel>()
        arrMyModel.add(MyModel("Test1","Athan"))
        arrMyModel.add(MyModel("Test2","Athan"))

        //create usual recycler Adapter with the difference that we have to specify the color dor the selcted item
        // setting the recycler view, the object collection, the adapter, the action button and menu, and the interface listener for action mode
        adapter = TestAdapter(arrMyModel)
        recyclerView = rvMyModel
        setArrObject(arrMyModel)
        setAdapter(adapter)
        actionMenu = R.menu.menu
        btnActionMenuDelete = R.id.action_delete
        listener = this
        implementRecyclerViewClickListeners();
    }




    fun implementRecyclerViewClickListeners(){

        rvMyModel.addOnItemTouchListener(RecyclerTouchListener(activity, rvMyModel, object : RecyclerClick_Listener{
            override fun onClick(view: View?, position: Int) {
                if (getmActionMode() != null)
                    onListItemSelect(position);
            }

            override fun onLongClick(view: View?, position: Int) {
                onListItemSelect(position);
            }
        }))


    }

    override fun onActionDeleteClicked( position: Int) {

        TangerineLog.log("Deleted: $position")
        dismissActionMode()
    }

}