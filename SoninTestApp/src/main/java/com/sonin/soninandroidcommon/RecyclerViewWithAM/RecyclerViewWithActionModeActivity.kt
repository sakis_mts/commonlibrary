package com.sonin.soninandroidcommon

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.sonin.soninandroidcommon.RecyclerViewWithAM.TestAdapter
import kotlinx.android.synthetic.main.main_fragment.*


class RecyclerViewWithActionModeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        val fragment = MainFragment()
        ft.replace(R.id.frame_container, fragment).commit()



    }
}
