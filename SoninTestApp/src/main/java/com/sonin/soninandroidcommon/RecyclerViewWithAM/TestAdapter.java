package com.sonin.soninandroidcommon.RecyclerViewWithAM;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sonin.soninandroidcommon.MyModel;
import com.sonin.soninandroidcommon.R;
import com.sonin.soninandroidcommonlib.recyclerView.RecyclerViewAdapterWIthActionMode;

import java.util.ArrayList;

/**
 * Created by athanasios.moutsioul on 18/09/2017.
 */

public class TestAdapter extends RecyclerViewAdapterWIthActionMode {


    private ArrayList<MyModel> arrObjectList;
    public static int ACTIVE        = 1;
    public static int CANCEL        = 2;
    public static int NOT_AVAILABLE = 3;


    public TestAdapter(ArrayList<MyModel> arrObjectList)
    {
        this.arrObjectList           = arrObjectList;
    }



    @Override
    public int getItemCount() {
        return arrObjectList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i)
    {
        MyModel myModel = arrObjectList.get(i);


        TestAdapter.MyModelViewHolder mandateViewHolder = (TestAdapter.MyModelViewHolder) viewHolder;
        mandateViewHolder.bindElement(myModel);
        mandateViewHolder.tvTitle.setText(myModel.getStrTitle());

        mandateViewHolder.itemView
                .setBackgroundColor(getmSelectedItemsIds().get(i) ? 0x9934B5E4
                        : Color.TRANSPARENT);

    }


    @Override
    public int getItemViewType(int position) {
        return 0;
    }


    /*

     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.listitem, viewGroup, false);

        return new TestAdapter.MyModelViewHolder(itemView);
    }


    public  class MyModelViewHolder extends RecyclerView.ViewHolder
    {


        protected TextView tvTitle;

        protected MyModel myModel;


        public MyModelViewHolder(View v) {
            super(v);

            this.tvTitle          = (TextView)          v.findViewById(R.id.tvTitle);


        }

        public void bindElement(MyModel myModel)
        {
            this.myModel = myModel;
        }


    }

    public void swap(ArrayList<MyModel> arrObjectList)
    {
        if(this.arrObjectList != null)
        {
            this.arrObjectList.clear();
            this.arrObjectList.addAll(arrObjectList);
            notifyDataSetChanged();
        }
    }

}
