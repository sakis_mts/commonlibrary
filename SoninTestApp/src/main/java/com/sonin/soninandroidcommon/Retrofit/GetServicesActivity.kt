package com.sonin.soninandroidcommon.Retrofit

import android.accounts.AccountManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidcommon.BuildConfig
import com.sonin.soninandroidcommon.R
import com.sonin.soninandroidcommon.models.Service
import com.sonin.soninandroidcommonlib.networking.CustomGsonConverterFactory
import com.sonin.soninandroidcommonlib.networking.RetrofitServiceGenerator
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GetServicesActivity : AppCompatActivity() {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    lateinit var networkService : NetworkRequestService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_services)
        val strAuthToken = SessionManager.instance.getAuthorizationAccessTokenType()
        networkService  = RetrofitServiceGenerator.getServiceUsingAuthorization(NetworkRequestService::class.java,  strAuthToken,"sakis", AccountManager.get(this), BuildConfig.ENDPOINT, CustomGsonConverterFactory.create(), getString(R.string.accountType) )
        requestServices()
    }

    private fun requestServices() {



        println(SessionManager.instance.getAccessToken())
        compositeDisposable.add(

                getServices()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            result ->

                            println(result)


                        }, { error ->
                            error.printStackTrace()
                        })
        )
    }

    fun getServices(): Observable<List<Service>> {
        return networkService.getService()

    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onPause() {
        compositeDisposable.dispose()
        super.onPause()
    }
}
