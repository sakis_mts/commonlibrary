package com.sonin.soninandroidcommon.Retrofit

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sonin.soninandroidcommon.R

class LogoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logout)
    }
}
