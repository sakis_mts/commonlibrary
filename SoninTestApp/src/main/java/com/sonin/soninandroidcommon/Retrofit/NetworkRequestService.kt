package com.sonin.soninandroidcommon.Retrofit


import com.sonin.soninandroidcommon.models.Service
import com.sonin.soninandroidcommon.models.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

/**
 * Created by athanasios.moutsioul on 27/09/2017.
 */
interface NetworkRequestService {

    @GET("/service")
    fun getService(): Observable<List<Service>>

    @GET("api/customer/me")
    fun getUser( ): Observable<User>
}