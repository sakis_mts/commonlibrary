package com.sonin.soninandroidcommon.Retrofit

import android.accounts.AccountManager
import android.app.Application
import android.arch.lifecycle.LiveData
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidcommon.BuildConfig
import com.sonin.soninandroidcommon.R
import com.sonin.soninandroidcommon.database.AppDatabase
import com.sonin.soninandroidcommon.models.User
import com.sonin.soninandroidcommonlib.models.AccessToken
import com.sonin.soninandroidcommonlib.networking.CustomGsonConverterFactory
import com.sonin.soninandroidcommonlib.networking.OAuthService
import com.sonin.soninandroidcommonlib.networking.RetrofitServiceGenerator
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitActivity : AppCompatActivity() {

    lateinit var loginService : OAuthService

    val strGrandType                = "password"
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retrofit)

        SessionManager.init(this, ServiceActivity::class.java, LogoutActivity::class.java,"")

        loginService    = RetrofitServiceGenerator.getServiceWithoutUsingAuthorization(OAuthService::class.java, BuildConfig.ENDPOINT )


        logingRequest()



    }

    private fun logingRequest(){

        val gson = Gson()
        compositeDisposable.add(
                getAccessToken()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->

                            val json                            = gson.toJson(result)

                           SessionManager.instance.setAccount(this,result,getString(R.string.accountType),"sakis", true,false)

                        }, { error ->
                            Log.d("Error", " Errorr ")
                            error.printStackTrace()
                        })


        )

    }



//    fun setAccount( objAccessToken: AccessToken){
//
//        val account = AccountGeneral.isAccountExists(AccountManager.get(this), "athan", getString(R.string.accountType))
//
//        if (account == null){
//
//            AccountGeneral.createAccount(this, AccountManager.get(this),"athan", objAccessToken,getString(R.string.accountType) )
//
//        }else{
//
//            AccountGeneral.updateAccessToken(AccountManager.get(this),objAccessToken, "athan",getString(R.string.accountType))
//
//            val sessionManager = SessionManager.instance
//            sessionManager.setLoggedInUsername("athan")
//
//            val intExpires = objAccessToken.expires_in
//            sessionManager.login(this@RetrofitActivity, objAccessToken.access_token, objAccessToken.refresh_token, Integer.toString(intExpires))
//
//        }
//    }



    fun getAccessToken() : Observable<AccessToken> {

        return loginService.getAccessTokennObservable(strGrandType, "sakis", "testing", getString(R.string.oauth_client_id), getString(R.string.oauth_client_secret))

    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onPause() {
        compositeDisposable.dispose()
        super.onPause()
    }
}
