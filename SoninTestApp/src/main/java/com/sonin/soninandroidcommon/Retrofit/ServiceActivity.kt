package com.sonin.soninandroidcommon.Retrofit


import android.accounts.AccountManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidcommon.R
import com.sonin.soninandroidcommon.Retrofit.NetworkRequestService
import com.sonin.soninandroidcommonlib.networking.RetrofitServiceGenerator
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import com.sonin.soninandroidcommonlib.networking.CustomGsonConverterFactory
import com.sonin.soninandroidcommon.BuildConfig
import com.sonin.soninandroidcommon.database.AppDatabase
import com.sonin.soninandroidcommon.models.User
import com.sonin.soninandroidcommonlib.models.AccessToken

class ServiceActivity : AppCompatActivity() {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    lateinit var networkService : NetworkRequestService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)
        val strAuthToken = SessionManager.instance.getAccessTokenType() + " " + SessionManager.instance.getAccessToken()
        networkService  = RetrofitServiceGenerator.getServiceUsingAuthorization(NetworkRequestService::class.java,  strAuthToken,"sakis", AccountManager.get(this), BuildConfig.ENDPOINT, CustomGsonConverterFactory.create(), getString(R.string.accountType) )
        requestServices()
        //testRequest()
    }

//    private fun testRequest(){
//
//        val gson = Gson()
//
//
//        var accessToken2 = AccessToken("Bearer",31536000,"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdlM2FiMTkzM2QxZDM1ODcwYTI1NTk1NzcyMTlkNmNiYzliOGYxOWI2MDI5MjdlMjcxNzM1MzNmYWM1MDQxNDlmMGI1MGU4NDc1ZmYyMDA2In0.eyJhdWQiOiIzIiwianRpIjoiN2UzYWIxOTMzZDFkMzU4NzBhMjU1OTU3NzIxOWQ2Y2JjOWI4ZjE5YjYwMjkyN2UyNzE3MzUzM2ZhYzUwNDE0OWYwYjUwZTg0NzVmZjIwMDYiLCJpYXQiOjE1MTE1MjY4NDYsIm5iZiI6MTUxMTUyNjg0NiwiZXhwIjoxNTQzMDYyODQ2LCJzdWIiOiIxMzgiLCJzY29wZXMiOltdfQ.pQHIuO2lnk359wJGbLScuvhI-TgfAdi6kvE1os5b4BT6Zkgni8ctnFVkQgT1tF1fveBLtgE7Rbd6jwdQE7vbjc86v7sBjCVJZLFCwBCXHAYfTYPK6UN3EQRDLY8Ow_XqI3Qc8DhwqfOGwDKwamm0jYWsMDxQdtIEwdTeAk5RROgKY76-w86dpVaXpPjNoFV86iVmdanYus_fDVOYMJZtw58AojaNCmVJsQhjAZNteSC5HW3e8A46IC9u2AX7wdhZBtWnN_hVs8WU16mT0Pa03MgdxMuprRH1KVs3BLf8K0CiLKzn0iaF-2HkTQYlzmTEJDkwtfLj79jLDI7vJ1GXiHVIP1I3ppyI30ZReSaWxb1nQDTWYASRJLGkxaV6iLl8jjRew2DSg2Bo0eB-Iisz5XzOIf5zNFCDUFj9pGfXeGKiL8HghJVE3pMxY9C0XeiES71MDu_mUVP0vlNO0GFVD1k2sQf0F8k6Hd8wEQCh66uOCoMDz5PAYd_BdAwGgT0yKZcVakvzBZpxo4BUPEQ0kvLgnzyu80cdRTSc1jzSkyFvDFh2LdqELgtKGP4lIMixP42YUe0rWP7EZdUkuHvn7Di6FRPENvytHoeEwf793sS0Us9jWxSL60l2ebsHPVDGEV37qQ9FPMzu70pV6olU5yAlfbIJKGHPnaLmiNt-y68","def50200a4680a92266041c0dcdcd272eb83097c0c8d71df47dbd50134b2ecce5534a70fe2ac7781ad9757f5f3c45d206115c003f04f1c0eb0f304325dadb96f4cccec7dba766dd6aaf9de6cf09f447e4eee861b8c7ffca679c7466f72c210a3e909987dbf2ca62a8866ddaf4ef6756e2cdba2ba87cded6b8fa1395ad8527b0fc2f176ee5345fc33501dea12ce39faa17ebd51713178ec175f896fa2e80f6d015948e15ef812cb0178a0126a081becfe2b82a5dff7cde83b7fcd095c174931c13e6eca44e3bc0010945dfd708c76cb556aa61695e338e488bf4d45360445fb5841c5075dec3ae516d49d7a93c1647adbb88e7a9f452a42e9fe6dfdbf9a900403c0c135271d85204c728a0a21b6faed7dd6083a89cedb66d6ab33ab087c861b2a21938f00de34b76d1a2fa460f854ebbfd65ce7aa7a5900df98fd85a5e4c6fbb002e01cf391a147583542c2f207c9643bc4b7b9895a4f6024405b610b9399b1d1a903a3" )
//        accessToken2.access_token ="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUyZGE1M2E0MzkxYjdiYjM1MDBiMDg4ZWQ1OTdhNTNlZjJlN2E5ODBjNzI1YWVjZTY5NjhiN2Y3MjI1NmVkMzIzOTJlOGM0ZTQyM2YzZDE1In0.eyJhdWQiOiIzIiwianRpIjoiZTJkYTUzYTQzOTFiN2JiMzUwMGIwODhlZDU5N2E1M2VmMmU3YTk4MGM3MjVhZWNlNjk2OGI3ZjcyMjU2ZWQzMjM5MmU4YzRlNDIzZjNkMTUiLCJpYXQiOjE1MTE1MjU5NDEsIm5iZiI6MTUxMTUyNTk0MSwiZXhwIjoxNTQzMDYxOTQxLCJzdWIiOiIxMzQiLCJzY29wZXMiOltdfQ.U-HwG6Usz7TPgpVhan0act_ZLkJQQsYlHTm7rWZlspxnvnjVGQN4cY4GaKMwVl-bt1x9NJGcsjl0nJiNdpzI8hsYVgtDUrZMbRCJjEV_srN2sdQZZHUCWlnZoEBNUgDRUDh1HoGhfUXYpiUFnMfyH-6pxYonkgtTqVn8wAnsUn9ynrZEyjObnoLk_LnL-AvFNRF2p94VPq65xQtMZGyC9Twb30RREi06iiiD_v-VX6awiUGWKoKq3lkSIKuYb2cgAXj1qsoGtTZ8Q_2XxLTajd1tBMleF4M_myg5-Vti3epSCCgs0kf0Ex7anJVVCVF6m0_nXpaiwZ4uvG31H_wB3LfJU8eefKrBEyvRFhx2SQ_C8DnbQuiZUQIg7PBLkRUbBYxgjbss7k6OMG3S2yNh67MiBz3v8Yw5cvU_wEXZs_ZpJqtAPbD-ZFBb4mT4jFOVBheNpOVoh43-KebWWzJTULb-IpuBqFXfUfqSbsD8Fhwqo5FMTpvYNt70wDGDJcU7ICWP2aIfS6VJLlLM_FfQddrfZNUBH3HXScDK52-3xssV_JNBq-zTKQkVxOPXB7ZRvBFMEStVDwuAZk1nvVkg2olAmBplKWTQColuDfazTlesIU_3CPsJSorcr3XSEe7I6nnFnleIennzNtxjXi60jUraQTk292HePu-2EIejkQU"
//        compositeDisposable.add(
//                getUser(accessToken2)
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribeOn(Schedulers.io())
//                        .subscribe ({
//                            result ->
//                            println(result)
//                            //insertItem(result)
//
//                        }, { error ->
//                            Log.d("Error", " Errorr ")
//                            error.printStackTrace()
//                        })
//
//
//        )
//    }


//    private fun getUser(accessT: AccessToken) : Observable<User>{
//
//
//        val networkService2  = RetrofitServiceGenerator.getServiceUsingAuthorization(NetworkRequestService::class.java, this, accessT.access_token,"sakis", AccountManager.get(this), BuildConfig.ENDPOINT2, CustomGsonConverterFactory.create(), getString(R.string.accountType) )
//
//        return networkService2.getUser()
//    }



    private fun requestServices() {

        println(SessionManager.instance.getAccessToken())
        compositeDisposable.add(

                getServices()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            result ->

                            println(result)


                        }, { error ->
                            error.printStackTrace()
                        })
        )
    }

    fun getServices(): Observable<List<com.sonin.soninandroidcommon.models.Service>> {
        return networkService.getService()

    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onPause() {
        compositeDisposable.dispose()
        super.onPause()
    }
}
