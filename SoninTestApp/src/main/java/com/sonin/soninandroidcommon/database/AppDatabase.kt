package com.sonin.soninandroidcommon.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.sonin.soninandroidcommonlib.database.AccessTokenModelDao
import com.sonin.soninandroidcommonlib.models.AccessToken
import com.sonin.soninandroidcommonlib.utils.TangerineLog

@Database(entities = arrayOf(AccessToken::class), version = 1)
abstract class AppDatabase : RoomDatabase() {



    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "testTracker_db").build()
            }
            TangerineLog.log("dbInstance: ${INSTANCE}")
            return INSTANCE!!
        }
    }

    abstract fun accessTokenModel() : AccessTokenModelDao


}