package com.sonin.soninandroidcommon.models

import com.google.gson.annotations.SerializedName

/**
 * Created by athanasios.moutsioul on 27/09/2017.
 */
class Service( @SerializedName("idService") val idService : Int)