package com.sonin.soninandroidcommon.models

/**
 * Created by athanasios.moutsioul on 24/11/2017.
 */
import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by athanasios.moutsioul on 19/10/2017.
 */
@Entity(tableName = "User")
data class User (

        @SerializedName("id")
        var id :Int,

        @SerializedName("type")
        var type :String,

        @SerializedName("email")
        var email :String,

        @SerializedName("first_name")
        var first_name :String,

        @SerializedName("last_name")
        var last_name :String,

        @SerializedName("company_name")
        var company_name :String,

        @SerializedName("job_title")
        var job_title :String,

        @SerializedName("phone_number")
        var phone_number :String,

        @SerializedName("password")
        var password :String

)