package com.sonin.soninandroidcommon.phoneView

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import com.sonin.soninandroidcommon.R
import com.sonin.soninandroidcommonlib.customViews.PhoneView
import com.sonin.soninandroidcommonlib.validation.DefaultNotEmptyPolicy
import com.sonin.soninandroidcommonlib.validation.Field
import com.sonin.soninandroidcommonlib.validation.Form
import kotlinx.android.synthetic.main.activity_phone_view.*
import org.jetbrains.anko.find

class PhoneViewActivity : AppCompatActivity() {

    private lateinit var tmp: PhoneView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_view)

        val  form =  Form()
        form.addField(Field.FieldBuilder(customPhoneView, Field.FieldType.PHONE).policy(DefaultNotEmptyPolicy()).requestKey("phonenumber").createField())

        //customPhoneView.setRegionFromPhone()
        customPhoneView.setRegionFromPhoneNumber("+447821423139")
        btnValidate.setOnClickListener {form.validateForm()
        println(form.toJSON())}



    }
}
