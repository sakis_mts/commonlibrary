package com.sonin.soninandroidcommonlib.accountManager

import android.accounts.Account
import android.accounts.AccountManager
import android.app.Activity
import android.content.Context
import com.sonin.android_common.networking.SessionManager
import com.sonin.soninandroidcommonlib.models.AccessToken
import com.sonin.soninandroidcommonlib.utils.TangerineLog
import java.util.ArrayList

class AccountGeneral {


    companion object {

        val AUTHTOKEN_TYPE_FULL_ACCESS          = "Full access"
        val AUTHTOKEN_TYPE_READ_ONLY            = "Read only"
        val AUTHTOKEN_TYPE_READ_ONLY_LABEL      = "Read only access to an account"
        val AUTHTOKEN_TYPE_FULL_ACCESS_LABEL    = "Full access to an account"
        val ARG_ACCOUNT_TYPE                    = "ACCOUNT_TYPE"
        val ARG_AUTH_TYPE                       = "AUTH_TYPE"
        val ARG_ACCOUNT_NAME                    = "ACCOUNT_NAME"
        val ARG_IS_ADDING_NEW_ACCOUNT           = "IS_ADDING_ACCOUNT"


        /**
         * @param mAccountManager
         * @param account
         * @param authTokenType
         * @param activity
         * * Get the  authorization token of an account from Account Manager
         */
        fun getExistingAccountAuthToken(mAccountManager: AccountManager, account: Account, authTokenType: String) {

            val future = mAccountManager.getAuthToken(account, authTokenType, null, null, null, null)

            Thread(Runnable {
                try {

                    val bnd         = future.result
                    val authtoken   = bnd.getString(AccountManager.KEY_AUTHTOKEN)


                } catch (e: Exception) {

                    e.printStackTrace()

                }
            }).start()

        }

        /**
         * @param mAccountManager
         * @param account
         * @param authTokenType
         * @param activity
         * * Invalidate the AuthToken for an existing user
         */
        fun invalidateAuthToken(mAccountManager: AccountManager, account: Account, authTokenType: String) {

            val future = mAccountManager.getAuthToken(account, authTokenType, null,null, null, null)

            Thread(Runnable {
                try {

                    val bnd         = future.result
                    val authtoken   = bnd.getString(AccountManager.KEY_AUTHTOKEN)

                    mAccountManager.invalidateAuthToken(account.type, authtoken)
                    AccountGeneral.getExistingAccountAuthToken(mAccountManager, account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS)

                } catch (e: Exception) {

                    e.printStackTrace()

                }
            }).start()
        }

        /**
         * @param mAccountManager
         * @param strUserName
         * @param accountType
         * @return
         * * Retrieve the username of an existing account
         */
        fun getAccountFromUsername(mAccountManager: AccountManager, strUserName: String, accountType: String): Account? {

            val arrAccounts = mAccountManager.accounts

            if (arrAccounts.size > 0) {

                for (account in arrAccounts) {

                    if (account.type.equals(accountType)) {

                        if (account.name.equals(strUserName.toString().trim())) {

                            //update the access token for this account
                            return account

                        }
                    }

                }

            }

            return null

        }


        /**
         * @param context
         * @param mAccountManager
         * @param strAccountName
         * @param accessToken
         * @param accountType
         * * create a new acount for the app
         */
        fun createAccount(context: Context, mAccountManager: AccountManager, strAccountName: String, accessToken: AccessToken, accountType: String, blnNavigateToHome: Boolean, blnStayLoggedIn: Boolean) {


            val authtoken       = accessToken.access_token
            val authtokenType   = AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS
            val account         = Account(strAccountName, accountType)
            val blnAccount      = mAccountManager.addAccountExplicitly(account, accessToken.refresh_token, null)

            if (blnAccount) {

                mAccountManager.setAuthToken(account, authtokenType, authtoken)

                val sessionManager = SessionManager.instance

                sessionManager.setLoggedInUsername(strAccountName)
                sessionManager.login(context, blnNavigateToHome,blnStayLoggedIn)


            } else {

                TangerineLog.log("Failed to create account")

            }


        }

        /**
         * @param mAccountManager
         * @param strName
         * @param accountType
         * @return
         * * check if the account exists in the Account Manager
         */
        fun isAccountExists(mAccountManager: AccountManager, strName: String, accountType: String): Account? {

            val arrAccounts         = mAccountManager.accounts
            val arrAccountNames     = ArrayList<String>()

            if (arrAccounts.size > 0) {

                for (account in arrAccounts) {

                    if (account.type.equals(accountType)) {

                        arrAccountNames.add(account.name)

                        if (account.name.equals(strName) ) {

                            return account

                        }

                    }

                }

            } else {
                // the array account is empty
                return null

            }

            return null
        }

        /**
         * @param mAccountManager
         * @param accessToken
         * @param strAccountName
         * @param accountType
         * *
         * * find the correct account through account manager and set the auth token
         */
        fun updateAccessToken(mAccountManager: AccountManager, accessToken: AccessToken, strAccountName: String, accountType: String) {

            val arrAccounts = mAccountManager.accounts

            if (arrAccounts.size > 0) {

                for (account in arrAccounts) {

                    if (account.type.equals(accountType)) {

                        if (account.name.equals(strAccountName.trim()) ) {

                            //update the access token for this account
                            mAccountManager.setAuthToken(account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, accessToken.access_token)
                           mAccountManager.setPassword(account, accessToken.refresh_token)

                        }
                    }

                }

            }
        }

    }

}
