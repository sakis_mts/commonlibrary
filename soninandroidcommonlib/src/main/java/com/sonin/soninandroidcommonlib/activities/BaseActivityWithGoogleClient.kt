package com.sonin.soninandroidcommonlib.activities

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.annotation.SuppressLint
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import android.content.pm.PackageManager
import android.support.annotation.NonNull
import com.sonin.soninandroidcommonlib.utils.PermissionManager
import com.google.android.gms.common.ConnectionResult
import android.os.Bundle
import android.support.annotation.Nullable
import com.google.android.gms.location.LocationListener;
import android.location.Location;

open class BaseActivityWithGoogleClient : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, PermissionManager.PermissionListener {


    public var mGoogleApiClient: GoogleApiClient?  = null
    private var mLocationRequest: LocationRequest?  = null
    private val REQUEST_CODE_LOCATION_PERMISSION    = 2
    private val PERMISSIONS_LOCATION                = arrayOf<String>(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    var UPDATE_INTERVAL_IN_MILLISECONDS: Long       = 1000
    var FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS     = UPDATE_INTERVAL_IN_MILLISECONDS / 2

    override fun onStop() {
        super.onStop()

        mGoogleApiClient?.let {  it.disconnect() }


    }

    override fun onPause() {
        super.onPause()

        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        mGoogleApiClient?.let {

            if (it.isConnected()) {
                stopLocationUpdates()
            }
        }

    }

    override fun onResume() {
        super.onResume()

        mGoogleApiClient?.let {

            if (it.isConnected()) {

                startLocationUpdates()

            }

        }
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        buildGoogleApiClient()

    }

    override fun onStart() {

        mGoogleApiClient?.let {  it.connect() }
        super.onStart()
    }


    /**
     * Builds a GoogleApiClient. Uses the `#addApi` method to request the
     * LocationServices API.
     */
    @Synchronized protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        createLocationRequest()
    }

    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest!!.interval = UPDATE_INTERVAL_IN_MILLISECONDS

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest!!.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS

        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    fun changeLocationRequestFrequency(interval: Long = 10000, fastInterval: Long = 5000){

        this.UPDATE_INTERVAL_IN_MILLISECONDS            = interval
        this.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS    =fastInterval
        mLocationRequest!!.interval                     = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest!!.fastestInterval              = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS

    }



    override fun onConnected(p0: Bundle?) {

        println("The Google Api client is connected")
        PermissionManager.requestPermissionsIfNecessary(this, PERMISSIONS_LOCATION, this, REQUEST_CODE_LOCATION_PERMISSION)
        startLocationUpdates()

    }
    override fun onConnectionSuspended(i: Int) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        mGoogleApiClient?.let { it.connect() }

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

        println("The Google Api client is failed")

    }

    protected fun startLocationUpdates() {

        PermissionManager.requestPermissionsIfNecessary(this, PERMISSIONS_LOCATION, this, REQUEST_CODE_LOCATION_PERMISSION)

    }

    override fun onLocationChanged(location: Location) {

    }

    protected fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
    }


    @SuppressLint("MissingPermission")
    override fun onPermissionAlreadyGranted() {

        mGoogleApiClient?.let {

            if (it.isConnected()) {

                LocationServices.FusedLocationApi.requestLocationUpdates(it, mLocationRequest, this)
            }
        }

    }

    override fun onExplainPermission() {

    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {

            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                mGoogleApiClient?.let {  LocationServices.FusedLocationApi.requestLocationUpdates(it, mLocationRequest, this) }



            } else {

                onExplainPermission()
            }
        }
    }


}