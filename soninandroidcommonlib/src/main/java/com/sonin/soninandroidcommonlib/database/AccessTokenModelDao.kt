package com.sonin.soninandroidcommonlib.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.sonin.soninandroidcommonlib.models.AccessToken

@Dao
interface AccessTokenModelDao {

    @Query("select * from AccessToken")
    fun getAllAccessTokensItems(): LiveData<List<AccessToken>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAccessToken(accessTokenModel: AccessToken)

    @Delete
    fun deleteAccessToken(accessTokenModel: AccessToken)
}