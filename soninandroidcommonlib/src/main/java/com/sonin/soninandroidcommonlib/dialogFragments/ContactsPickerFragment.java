package com.sonin.soninandroidcommonlib.dialogFragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.sonin.soninandroidcommonlib.adapters.ContactsCursorAdapter;
import com.sonin.soninandroidcommonlib.R;

@SuppressLint("InlinedApi")
public class ContactsPickerFragment extends DialogFragment implements LoaderCallbacks<Cursor>,
		OnClickListener  {

	private ContactsPickedListener listener;
	private ContactsCursorAdapter    adapter;
	private ListView               lvContacts;
	private Button                 bContactPickerFinished;
	
	private static final String[] PROJECTION = new String[] {
		    ContactsContract.Contacts._ID,
		    Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
            ContactsContract.Contacts.DISPLAY_NAME,
		    ContactsContract.CommonDataKinds.Email.DATA };
	
	private String order = "CASE WHEN " 
            + ContactsContract.Contacts.DISPLAY_NAME 
            + " NOT LIKE '%@%' THEN 1 ELSE 2 END, " 
            + ContactsContract.Contacts.DISPLAY_NAME 
            + ", " 
            + ContactsContract.CommonDataKinds.Email.DATA
            + " COLLATE NOCASE";
    private String filter = ContactsContract.CommonDataKinds.Email.DATA + " != " +  (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
    																				 ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
    																			     ContactsContract.Contacts.DISPLAY_NAME);

	public interface ContactsPickedListener {
		void onContactsPicked(ArrayList<String> emails);
	}

	public static ContactsPickerFragment newInstance() {

		ContactsPickerFragment f = new ContactsPickerFragment();
		return f;

	}

	public ContactsPickerFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_contact_picker, container);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		initVars(view);
		loadContacts();
		return view;
	}
	

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		Dialog dialog;

		if (android.os.Build.VERSION.SDK_INT >= 11) {
			dialog = new Dialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
		}else{
			dialog = new Dialog(getActivity());
		}
		
		
		return dialog;
	}


	private void loadContacts() {

		getActivity().getSupportLoaderManager().initLoader(0, null, this);

	}

	private void initVars(View v) {

		listener                = (ContactsPickedListener) getActivity();
		lvContacts              = (ListView) v.findViewById(R.id.lvContactPicker);
		bContactPickerFinished  = (Button)   v.findViewById(R.id.bContactPickerFinished);

		
		
        adapter = new ContactsCursorAdapter(getActivity(), null, 0);
        lvContacts.setEmptyView(v.findViewById(android.R.id.empty));
        lvContacts.setAdapter(adapter);
        
        bContactPickerFinished.setOnClickListener(this);


	}

	@Override
	public void onClick(View view) {
		
		ArrayList<String> emailList = new ArrayList<String>();
		Cursor cursor = adapter.getCursor();
		
		int[] checkedValues = adapter.getCheckedValues();
		
		for(int i = 0; i<checkedValues.length; i++){
			if(checkedValues[i]==1){
				cursor.moveToPosition(i);
				emailList.add(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)));
			}
		}
		
		listener.onContactsPicked(emailList);
		dismiss();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		
		
		return new CursorLoader(getActivity(), ContactsContract.CommonDataKinds.Email.CONTENT_URI,
				PROJECTION, filter, null, order);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		
		adapter.setListSize(data.getCount());
		adapter.swapCursor(data);
		
		
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		
		
		adapter.swapCursor(null);
		
	}

	
	
	

}
