package com.sonin.soninandroidcommonlib.dialogFragments;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	public static final String BUNDLE_KEY_CODE = "bundlekeycode";
	public static final String BUNDLE_KEY_TITLE = "bundlekeytitle";
	public static final String BUNDLE_KEY_TIME = "bundlekeytime";

	public static final String RETURN_CODE_START_DATE = "0";
	public static final String RETURN_CODE_END_DATE = "1";

	private DatePickedListener listener;
	private GregorianCalendar  calendar;
	private String title;
	private String code;
	private long time;

	public interface DatePickedListener {
		void onStartDatePicked(GregorianCalendar calendar);

		void onEndDatePicked(GregorianCalendar calendar);
	}

	public static DatePickerFragment newInstance(String returnCode, String title, long time) {

		Bundle args = new Bundle();
		args.putString(BUNDLE_KEY_CODE, returnCode);
		args.putString(BUNDLE_KEY_TITLE, title);
		args.putLong(BUNDLE_KEY_TIME, time);
		DatePickerFragment f = new DatePickerFragment();
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setStyle(DialogFragment.STYLE_NO_FRAME, 0);
		listener = (DatePickedListener) getActivity();
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		title = "";
		if (getArguments() != null) {

			if (getArguments().containsKey(BUNDLE_KEY_CODE)) {
				code = getArguments().getString(BUNDLE_KEY_CODE);
			}

			if (getArguments().containsKey(BUNDLE_KEY_TITLE)) {
				title = getArguments().getString(BUNDLE_KEY_TITLE);
			}

			if (getArguments().containsKey(BUNDLE_KEY_TIME)) {
				time = getArguments().getLong(BUNDLE_KEY_TIME);
			}
		}

		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();
		if (time > 0) {
			c.setTimeInMillis(time);
		}

		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		DatePickerDialog datePickerDialog = null;
		if (android.os.Build.VERSION.SDK_INT >= 11) {
			// only for gingerbread and newer versions
			datePickerDialog = new DatePickerDialog(getActivity(),
					android.R.style.Theme_DeviceDefault_Dialog, this, year,
					month, day) {
				@Override
				public void onDateChanged(DatePicker view, int year, int month,
						int day) {
					super.onDateChanged(view, year, month, day);
					setTitle(title);
				}

				@Override
				public void onClick(DialogInterface dialog, int which) {
					super.onClick(dialog, which);
					
					if (code.equals(RETURN_CODE_START_DATE)) {
						listener.onStartDatePicked(calendar);
					} else {
						listener.onStartDatePicked(calendar);
					}
				}
				
				
			};
		} else {
			datePickerDialog = new DatePickerDialog(getActivity(), this, year,
					month, day) {
				@Override
				public void onDateChanged(DatePicker view, int year, int month,
						int day) {
					super.onDateChanged(view, year, month, day);
					setTitle(title);
				}
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					super.onClick(dialog, which);
					
					if (code.equals(RETURN_CODE_START_DATE)) {
						listener.onStartDatePicked(calendar);
					} else {
						listener.onStartDatePicked(calendar);
					}
				}
			};
		}

		datePickerDialog.setTitle(title);
		datePickerDialog.getWindow()
				.setBackgroundDrawable(new ColorDrawable(0));

		// Create a new instance of DatePickerDialog and return it
		return datePickerDialog;
	}

	public void onDateSet(DatePicker view, int year, int month, int day) {

		calendar = (GregorianCalendar) GregorianCalendar
				.getInstance();
		calendar.set(year, month, day, 0, 0, 0);
		calendar.set(Calendar.MILLISECOND, 0);

	

	}

}