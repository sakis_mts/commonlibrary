package com.sonin.soninandroidcommonlib.dialogFragments;



import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.sonin.soninandroidcommonlib.R;


public class Dialog_Yes_No extends DialogFragment implements
		OnClickListener {

	private TextView tvTitle;
	private TextView tvMainBody;
	private TextView tvSuccess;
	private TextView tvFailure;
	
	private String title;
	private String main;
	private String success;
	private String failure;
	
	public  static String  BUNDLE_KEY_TITLE   = "bundle_key_title";
	public  static String  BUNDLE_KEY_MAIN    = "bundle_key_main";
	public  static String  BUNDLE_KEY_SUCCESS = "bundle_key_success";
	public  static String  BUNDLE_KEY_FAILURE = "bundle_key_failure";

	public interface GenericDialogListener {
		void onDialogSuccess();
		void onDialogFailure();
	}

	public static Dialog_Yes_No newInstance(GenericDialogListener listener, String title, String text, String success, String failure) {
		
		Bundle args = new Bundle();
		args.putString(BUNDLE_KEY_TITLE ,title);
		args.putString(BUNDLE_KEY_MAIN, text);
		args.putString(BUNDLE_KEY_SUCCESS, success);
		args.putString(BUNDLE_KEY_FAILURE, failure);
		
		Dialog_Yes_No f = new Dialog_Yes_No();
		f.setArguments(args);
		if(listener instanceof Fragment){
			f.setTargetFragment((Fragment) listener, /* requestCode */1234);
		}
		
		return f;
	}

	public Dialog_Yes_No() {
		// Empty constructor required for DialogFragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.dialog_fragment_yesno, container);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		initVars(view);
		setData();
		return view;
	}

	

	private void initVars(View view) {

		Bundle args = getArguments();
		
		title   = args.getString(BUNDLE_KEY_TITLE);
		main    = args.getString(BUNDLE_KEY_MAIN);
		success = args.getString(BUNDLE_KEY_SUCCESS);
		failure = args.getString(BUNDLE_KEY_FAILURE);
		
		
		
		
		tvTitle    = (TextView) view.findViewById(R.id.tvDialogTitle);
		tvMainBody = (TextView) view.findViewById(R.id.tvDialogMainText);
		tvSuccess  = (TextView) view.findViewById(R.id.tvDialogSuccess);
		tvFailure  = (TextView) view.findViewById(R.id.tvDialogFailure);


		tvSuccess.setOnClickListener(this);
		tvFailure.setOnClickListener(this);
		
	}
	
	private void setData(){
		
		tvTitle.setText(title);
		tvMainBody.setText(main);
		tvSuccess.setText(success);
		tvFailure.setText(failure);

	}
	

	@Override
	public void onClick(View v) {
		v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
		int id = v.getId();
		if (id == R.id.tvDialogSuccess) {
			if(getTargetFragment()!=null){
				Fragment fragment = (Fragment) getTargetFragment();
				((GenericDialogListener) fragment).onDialogSuccess();
			}else{
				FragmentActivity activity = getActivity();
				((GenericDialogListener) activity).onDialogSuccess();
			}
			this.dismiss();
		} else if (id == R.id.tvDialogFailure) {
			if(getTargetFragment()!=null){
				Fragment fragment = (Fragment) getTargetFragment();
				((GenericDialogListener) fragment).onDialogFailure();
			}else{
				FragmentActivity activity = getActivity();
				((GenericDialogListener) activity).onDialogFailure();
			}
			this.dismiss();
		}

	}


	

}
