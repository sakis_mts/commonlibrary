package com.sonin.soninandroidcommonlib.recyclerView;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.View;


import com.sonin.soninandroidcommonlib.recyclerView.*;


import java.util.ArrayList;

public class FragmentRecyclerVIewWithActionMode extends Fragment  {

    private RecyclerView    recyclerView;
    private ActionMode      mActionMode;
    private int             actionMenu;
    private ArrayList<?>    arrObject;
    private int             btnActionMenuDelete;

    private RecyclerViewAdapterWIthActionMode                   adapter;
    private Toolbar_ActionMode_Callback.onActionButtonCLicked   listener;


    public void setAdapter(RecyclerViewAdapterWIthActionMode adapter) {

        this.adapter = adapter;
        initializeRecyclerView();

    }

    public void setArrObject(ArrayList<?> arrObject) {
        this.arrObject = arrObject;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {

        this.recyclerView = recyclerView;

    }

    public ActionMode getmActionMode() {
        return mActionMode;
    }

    public void setmActionMode(ActionMode mActionMode) {
        this.mActionMode = mActionMode;
    }


    public void initializeRecyclerView()
    {

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

    }

    //Implement item click and long click over recycler view
//    public void implementRecyclerViewClickListeners() {
//        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerClick_Listener() {
//            @Override
//            public void onClick(View view, int position) {
//                //If ActionMode not null select item
//                if (!arrCanceledMandates.contains(position)){ //for future proejects this if
//                    if (mActionMode != null)
//                        onListItemSelect(position);
//
//                }
//
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//                //Select item on long click
//                if (!arrCanceledMandates.contains(position)){
//                    onListItemSelect(position);
//                }
//            }
//        }));
//    }


    //List item select method
    public void onListItemSelect(int position) {

        adapter.toggleSelection(position);//Toggle the selection

        boolean hasCheckedItems = adapter.getSelectedCount() > 0;//Check if any items are already selected or not

        if (hasCheckedItems && mActionMode == null){
            // there are some selected items, start the actionMode
            mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(new Toolbar_ActionMode_Callback(getActivity(),adapter,
                                                                                        arrObject,
                                                                                        this,
                                                                                        getBtnActionMenuDelete(),
                                                                                        getActionMenu(),
                                                                                        listener));

        }else if (!hasCheckedItems && mActionMode != null){
            // there no selected items, finish the actionMode
            mActionMode.finish();

        }
        if (mActionMode != null)
            //set action mode title on item selection
            mActionMode.setTitle(String.valueOf(adapter.getSelectedCount()) + " item selected");


    }
    //Set action mode null after use
    public void setNullToActionMode() {

        if (mActionMode != null){

            mActionMode = null;

        }
    }

    //Delete selected rows
    public void deleteRows() {
        SparseBooleanArray selected = adapter.getSelectedIds();//Get selected ids

        //Loop all selected ids
        for (int i = (selected.size() - 1); i >= 0; i--) {

            if (selected.valueAt(i)) {
                //If current id is selected remove the item via key
                arrObject.remove(selected.keyAt(i));
                adapter.notifyItemRemoved(selected.keyAt(i));

            }
        }

        mActionMode.finish();//Finish action mode after use

    }


    public int getBtnActionMenuDelete() {
        return btnActionMenuDelete;
    }

    public void setBtnActionMenuDelete(int btnActionMenuDelete) {

        this.btnActionMenuDelete = btnActionMenuDelete;

    }

    public int getActionMenu() {
        return actionMenu;
    }

    public void setActionMenu(int actionMenu) {
        this.actionMenu = actionMenu;
    }

    public Toolbar_ActionMode_Callback.onActionButtonCLicked getListener() {
        return listener;
    }

    public void setListener(Toolbar_ActionMode_Callback.onActionButtonCLicked listener) {

        this.listener = listener;
    }

    public void  dismissActionMode(){

        mActionMode.finish();

    }

}
