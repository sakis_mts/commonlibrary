package com.sonin.soninandroidcommonlib.recyclerView;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RecyclerViewAdapterWIthActionMode extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public SparseBooleanArray getmSelectedItemsIds() {
        return mSelectedItemsIds;
    }

    private SparseBooleanArray mSelectedItemsIds;

    public RecyclerViewAdapterWIthActionMode() {

        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {}

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return null;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public RecyclerViewHolder(View v) {
            super(v);

        }

        public void bindElement() {

        }

    }
    /***
     * Methods required for do selections, remove selections, etc.
     */

    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {

        if (value){

            mSelectedItemsIds.clear();//if we want multiple selection delete this row
            mSelectedItemsIds.put(position, value);
        }
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }


}
