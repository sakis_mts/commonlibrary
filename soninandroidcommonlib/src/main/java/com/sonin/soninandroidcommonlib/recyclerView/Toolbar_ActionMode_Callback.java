package com.sonin.soninandroidcommonlib.recyclerView;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.view.ActionMode;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;

public class Toolbar_ActionMode_Callback implements ActionMode.Callback {

    public interface onActionButtonCLicked{

        void onActionDeleteClicked(int position);

    }

    private Context                 context;
    private ArrayList<?>            arrObjects;
    private boolean                 isListViewFragment;
    private Fragment                currentFragment;
    private int                     btnActionDelete;
    private int                     actionMenu;
    private boolean                 blnIsActivity;
    private onActionButtonCLicked   listener;

    private RecyclerViewAdapterWIthActionMode recyclerView_adapter;

    public Toolbar_ActionMode_Callback(Context context, RecyclerViewAdapterWIthActionMode recyclerView_adapter,
                                       ArrayList<?> arrMandates, Fragment currentFragment, int btnActionDelete,
                                       int actionMenu, onActionButtonCLicked listener) {

        this.context                = context;
        this.recyclerView_adapter   = recyclerView_adapter;
        this.arrObjects             = arrMandates;
        this.currentFragment        = currentFragment;
        this.btnActionDelete        = btnActionDelete;
        this.actionMenu             = actionMenu;
        this.blnIsActivity          = false;
        this.listener               = listener;

    }

    public Toolbar_ActionMode_Callback(Context context, RecyclerViewAdapterWIthActionMode adapter, ArrayList<?> arrObject,
                                       Activity activity, int btnActionDelete, int actionMenu,onActionButtonCLicked listener) {

        this.context                = context;
        this.recyclerView_adapter   = adapter;
        this.arrObjects             = arrObject;
        this.btnActionDelete        = btnActionDelete;
        this.actionMenu             = actionMenu;
        this.blnIsActivity          = true;
        this.listener               = listener;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(actionMenu, menu);//Inflate the menu over action mode
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        //Sometimes the meu will not be visible so for that we need to set their visibility manually in this method
        //So here show action menu according to SDK Levels
        if (Build.VERSION.SDK_INT < 11) {

            MenuItemCompat.setShowAsAction(menu.findItem(btnActionDelete), MenuItemCompat.SHOW_AS_ACTION_NEVER);

        } else {

            menu.findItem(btnActionDelete).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        }

        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

        if (item.getItemId() == btnActionDelete){

            SparseBooleanArray selected = recyclerView_adapter
                    .getSelectedIds();//Get selected ids

            int selected_item_position = -1;
            //Loop all selected ids
            for (int i = (selected.size() - 1); i >= 0; i--) {

                if (selected.valueAt(i)) {
                    //If current id is selected change the status the item via key
                    selected_item_position = selected.keyAt(i);

                }
            }

            if (blnIsActivity){

               if (this.listener!=null){

                   this.listener.onActionDeleteClicked(selected_item_position);


               }

            }else{

                if (currentFragment != null) {

                    if (this.listener!=null){

                        this.listener.onActionDeleteClicked(selected_item_position);

                    }
                } else {

                    try {

                        FragmentRecyclerVIewWithActionMode fragment = new FragmentRecyclerVIewWithActionMode();

                        if (this.listener!=null){

                            this.listener.onActionDeleteClicked(selected_item_position);

                        }
                    } catch (Exception e) {

                        e.printStackTrace();

                    }
                }
            }

        }
        return false;
    }


    @Override
    public void onDestroyActionMode(ActionMode mode) {

        recyclerView_adapter.removeSelection();  // remove selection

        if (currentFragment != null) {

            ((FragmentRecyclerVIewWithActionMode) currentFragment).setNullToActionMode();//Set action mode null

        }

    }
}
