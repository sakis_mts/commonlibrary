package com.sonin.soninandroidcommonlib.security;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtils {

	private EncryptionUtils ()
	{}


	public static String xorMessage(String message, String key) {
		try {
			if (message == null || key == null)
				return null;

			char[] keys = key.toCharArray();
			char[] mesg = message.toCharArray();

			int ml = mesg.length;
			int kl = keys.length;
			char[] newmsg = new char[ml];

			for (int i = 0; i < ml; i++) {
				newmsg[i] = (char)  (mesg[i] ^  keys[i % kl]);
			}
			mesg = null;
			keys = null;
			return new String(newmsg);
		} catch (Exception e) {
			return null;
		}
	}
	
	/*Encrypts the message using AES in mode CBC with the given key and padding
		CBC uses an initialization vector which is appended on to the resulting encrypted message at the beginning
		The server knows to strip the first 128 bits as and use this to decrypt the message*/
	public static byte[] aesEncrypt(String message, String key, String padding)
	{
	 try {
		SecretKeySpec keyspec = new SecretKeySpec(hexStringToByteArray(key), "AES");
		SecureRandom rnd = new SecureRandom();
		IvParameterSpec iv = new IvParameterSpec(rnd.generateSeed(16));
		
		message = padString(message);
		
		
		 Cipher cipher = Cipher.getInstance("AES/CBC/" + padding);
         cipher.init(Cipher.ENCRYPT_MODE, keyspec, iv);
         
         byte[] combined = combineByteArrays(iv.getIV(),cipher.doFinal(message.getBytes("UTF-8")));
        
			return combined;
		} catch (Exception e) {
			e.printStackTrace();
		} 
        
		return null;
	}
	
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	
	  private static String padString(String source)
      {
        char paddingChar = ' ';
        int size = 16;
        int x = source.length() % size;
        int padLength = size - x;

        for (int i = 0; i < padLength; i++)
        {
                source += paddingChar;
        }

        return source;
      }

	  
	  public static byte[] combineByteArrays(byte[] one, byte[] two)
	  {
	         
	     byte[] combined = new byte[one.length + two.length];

         for (int i = 0; i < combined.length; ++i)
         {
             combined[i] = i < one.length ? one[i] : two[i - one.length];
         }
         
         return combined;
	  }
}
