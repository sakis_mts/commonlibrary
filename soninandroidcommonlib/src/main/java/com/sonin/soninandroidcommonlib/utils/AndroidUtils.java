package com.sonin.soninandroidcommonlib.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ScaleXSpan;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidUtils {


	private AndroidUtils()
	{
	}

	public static float convertToPix(Context context, float sizeInDips) {
		float scale = context.getResources().getDisplayMetrics().density;
		float size = sizeInDips * scale;
		return size;
	}

	public static int dpToPx(double dp) {
		return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static void setFont(View textView, String fontName) {
		Typeface tf = Typeface.createFromAsset(textView.getContext()
				.getAssets(), "fonts/" + fontName);

		if (textView instanceof TextView) {
			((TextView) textView).setTypeface(tf);
		} else if (textView instanceof Button) {
			((Button) textView).setTypeface(tf);
		}

	}

	public static void setActivityEnabled(Context context, final Class<? extends Activity> activityClass, final boolean enable) {
		final PackageManager pm = context.getPackageManager();
		final int enableFlag = enable ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
		pm.setComponentEnabledSetting(new ComponentName(context, activityClass), enableFlag, PackageManager.DONT_KILL_APP);
	}

	public static void setActivityAliasEnabled(Context context, String strAlias, boolean blnEnable) {
		final PackageManager pm = context.getPackageManager();
		final int enableFlag = blnEnable ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
		pm.setComponentEnabledSetting(new ComponentName(context, strAlias), enableFlag, PackageManager.DONT_KILL_APP);
	}

	public static boolean isValidUrl(String url) {
		Pattern p = Patterns.WEB_URL;
		Matcher m = p.matcher(url);
		if(m.matches())
			return true;
		else
			return false;
	}


	public static String addToString(String source, String target) {
		if (!TextUtils.isEmpty(source)) {
			target += source;
		}

		return target;
	}

	public static boolean isNetworkConnected(Context c) {
		ConnectivityManager cm = (ConnectivityManager) c
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null;
	}

	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	public static long getUTCTime(long localTime) {
		TimeZone tz = TimeZone.getDefault();
		int currentOffsetFromUTC = tz.getRawOffset();
		if (tz.inDaylightTime(new Date(localTime))) {
			currentOffsetFromUTC += tz.getDSTSavings();
		}
		return localTime + currentOffsetFromUTC;
	}

	public static Spannable applyKerning(CharSequence src, float kerning) {
		return applyKerning(src, kerning, 0, src.length());
	}

	public static Spannable applyKerning(CharSequence src, float kerning,
			int start, int end) {
		if (src == null)
			return null;
		final int srcLength = src.length();
		if (srcLength < 2)
			return src instanceof Spannable ? (Spannable) src
					: new SpannableString(src);
		if (start < 0)
			start = 0;
		if (end > srcLength)
			end = srcLength;

		final String nonBreakingSpace = "\u00A0";
		final SpannableStringBuilder builder = src instanceof SpannableStringBuilder ? (SpannableStringBuilder) src
				: new SpannableStringBuilder(src);
		for (int i = src.length(); i >= 1; i--) {
			builder.insert(i, nonBreakingSpace);
			builder.setSpan(new ScaleXSpan(kerning), i, i + 1,
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}

		return builder;
	}



	public static Notification buildNotification(Context ctx, String title,
			String message, int icon, PendingIntent pendingIntent) {

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				ctx).setSmallIcon(icon).setContentTitle(title)
				.setContentText(message).setContentIntent(pendingIntent); // Required
																			// on
																			// Gingerbread
																			// and
																			// below

		return mBuilder.build();

	}

	public static Bitmap takeScreenshot(ViewGroup viewGroup) {

		viewGroup.setDrawingCacheEnabled(true);
		viewGroup.buildDrawingCache();
		Bitmap drawingCache = viewGroup.getDrawingCache(true);
		Bitmap bitmap = Bitmap.createBitmap(drawingCache);
		viewGroup.setDrawingCacheEnabled(false);
		return bitmap;
	}

	public static Bitmap bitmapFromView(View v) {
		v.measure(MeasureSpec.makeMeasureSpec(v.getLayoutParams().width,
				MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
				v.getLayoutParams().height, MeasureSpec.EXACTLY));
		v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());

		Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		v.draw(c);

		return b;
	}

	public static Bitmap getBitmapFromView(Context context, View v) {
		DisplayMetrics dm = context.getApplicationContext().getResources()
				.getDisplayMetrics();
		v.measure(MeasureSpec.makeMeasureSpec(dm.widthPixels,
				MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
				dm.heightPixels, MeasureSpec.EXACTLY));
		v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
		Bitmap returnedBitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
				v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(returnedBitmap);
		v.draw(c);
		return returnedBitmap;
	}



	public static Intent newShareImageIntent(Uri imagePath, String optionalText) {

		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("image/*");
		share.putExtra(Intent.EXTRA_STREAM, imagePath);
		if (optionalText != null && !optionalText.isEmpty()) {
			share.putExtra(Intent.EXTRA_TEXT, optionalText);
		}

		return share;
	}
	
	

	public static Intent newEmailIntent(Context context, String address,
			String subject, String body, String cc) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}

	public static void longToast(Context context, String text) {
		Toast.makeText(context, text, Toast.LENGTH_LONG).show();
	}

	public static void shortToast(Context context, String text) {
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	public static void showKeyboard(final View v) {

		(new Handler()).postDelayed(new Runnable() {

			public void run() {

				v.dispatchTouchEvent(MotionEvent.obtain(
						SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
						MotionEvent.ACTION_DOWN, 0, 0, 0));
				v.dispatchTouchEvent(MotionEvent.obtain(
						SystemClock.uptimeMillis(), SystemClock.uptimeMillis(),
						MotionEvent.ACTION_UP, 0, 0, 0));

			}
		}, 200);
	}
	
	
	
	public static boolean validateEmail(String email){
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}
	
	@SuppressLint("NewApi")
	public static void runAsyncParallel(AsyncTask<String, String, String> task) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		} else {
			task.execute();
		}
	}
	
	
	public static String getFormattedDate(String timestamp, String format){
		Date date = new Date(Integer.parseInt(timestamp) *1000L); // *1000 is to convert seconds to milliseconds
		SimpleDateFormat sdf = new SimpleDateFormat(format); // the format of your date
		sdf.setTimeZone(TimeZone.getDefault());
		return sdf.format(date);
	}
	
	 public static void sendLocalBroadcast(Context context, Intent broadcastIntent){
		 
		 LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
		 localBroadcastManager.sendBroadcast(broadcastIntent);
	 }
	 
	public static void hideKeyboard(View v)
	{
		if(v != null)
		{
			InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		}
	}
		
public static void popFragmentBackstack(FragmentManager fm){

	int backStackCount = fm.getBackStackEntryCount();
	for (int i = 0; i < backStackCount; i++) {

	    int backStackId = fm.getBackStackEntryAt(i).getId();
	    fm.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);

	}
}

public static boolean hasApiVersion(int apiVersion){
	int currentVersion = android.os.Build.VERSION.SDK_INT;

	return currentVersion >= apiVersion;

}

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

	public static int getStatusBarHeight(Resources resources)
	{
		int result = 0;
		int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0)
		{
			result = resources.getDimensionPixelSize(resourceId);
		}
		return result;
	}











	
}
