package com.sonin.soninandroidcommonlib.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FilesystemUtils {

	private FilesystemUtils (){
	}
	
	public interface OnBitmapLoadedListener {
		void onBitmapLoaded(Bitmap bitmap);
	}
	
	public interface onBitmapSavedListener {
		void onBitmapSaved(Uri uri);
	}
	
	public interface onStringLoadedListener {
		void onStringLoaded(String string);
	}
	
	
	// Takes a folder name which is created on fly if doesn't exist
	public static void saveBitmapToFilesystem(final Bitmap bitmap, final String folderName,
			final String fileName, final onBitmapSavedListener listener) {

		
		new AsyncTask<Void, Void, Uri>(){

			@Override
			protected Uri doInBackground(Void... params) {
				try {
					String filePath = Environment.getExternalStorageDirectory()
							.getAbsolutePath() + "/" + folderName;

					File dir = new File(filePath);
					if (!dir.exists())
						dir.mkdirs();

					File file = new File(dir, fileName);
					FileOutputStream fOut = new FileOutputStream(file);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fOut);
					fOut.flush();
					fOut.close();

					return Uri.fromFile(file);
				} catch (Exception e) {
					e.printStackTrace();
					Log.i(null, "Save file error!");
				}

				return null;
			}
			
			@Override
			protected void onPostExecute(Uri result) {
				super.onPostExecute(result);
				if(listener!=null){
					listener.onBitmapSaved(result);
				}
			}
		}.execute();
	}

	
	public static void getBitmapFromFilesystem(final Context context, final Uri uri, final OnBitmapLoadedListener listener){
		new AsyncTask<Void, Void, Bitmap>(){

			@Override
			protected Bitmap doInBackground(Void... params) {
				
					
				Bitmap bitmap = null;
				try {
					bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return bitmap;
			}
			
			@Override
			protected void onPostExecute(Bitmap result) {
				super.onPostExecute(result);
				if(listener!=null){
					listener.onBitmapLoaded(result);
				}
			}
			
			
		}.execute();
	}
	
	
	public static void getStringFromAssets(final Context context, final String filename, final onStringLoadedListener listener){
		new AsyncTask<Void, Void, String>(){

			@Override
			protected String doInBackground(Void... params) {
				
					
				 AssetManager assetManager = context.getAssets();
				 ByteArrayOutputStream outputStream = null;
				 InputStream inputStream = null;
				 try {
				        inputStream = assetManager.open(filename);
				        outputStream = new ByteArrayOutputStream();
				        byte buf[] = new byte[1024];
				        int len;
				        try {
				            while ((len = inputStream.read(buf)) != -1) {
				                outputStream.write(buf, 0, len);
				            }
				            outputStream.close();
				            inputStream.close();
				        } catch (IOException e) {
				        	Log.wtf("TAG", e.toString());
				        }
				  } catch (IOException e) {
					  Log.wtf("TAG", e.toString());
				  }
				 
				 if(outputStream==null){
					 return "";
				 }
				    return outputStream.toString();

			}
			
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if(listener!=null){
					listener.onStringLoaded(result);
				}
			}
			
			
		}.execute();
	}

}
