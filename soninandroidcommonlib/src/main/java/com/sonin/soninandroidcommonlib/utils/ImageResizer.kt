package com.sonin.soninandroidcommonlib.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import java.io.ByteArrayOutputStream
import java.io.IOException
import android.media.ExifInterface
import android.provider.MediaStore

class ImageResizer {

    fun rotateImageIfRequired(context: Context, uri: Uri, fileBytes: ByteArray): ByteArray? {

        var data: ByteArray? = null
        var bitmap = BitmapFactory.decodeByteArray(fileBytes, 0, fileBytes.size)
        var outputStream: ByteArrayOutputStream? = null

        try {

            bitmap          = rotateImageIfRequired(bitmap, context, uri)
            outputStream    = ByteArrayOutputStream()

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            data = outputStream.toByteArray()

        } catch (e: IOException) {

            e.printStackTrace()

        } finally {
            try {

                if (outputStream != null) {
                    outputStream.close()
                }

            } catch (e: IOException) {
                // Intentionally blank
            }

        }

        return data
    }

    @Throws(IOException::class)
    fun rotateImageIfRequired(img: Bitmap, context: Context, selectedImage: Uri): Bitmap {

        if (selectedImage.scheme == "content") {

            val projection  = arrayOf(MediaStore.Images.ImageColumns.ORIENTATION)
            val c           = context.contentResolver.query(selectedImage, projection, null, null, null)

            if (c!!.moveToFirst()) {

                val rotation = c.getFloat(0)
                c.close()
                return rotateImage(img, rotation)

            }
            return img

        } else {

            val ei = ExifInterface(selectedImage.path)
            val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
            println("orientation: $orientation")

            when (orientation) {

                ExifInterface.ORIENTATION_ROTATE_90 -> return rotateImage(img, 90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> return rotateImage(img, 180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> return rotateImage(img, 270F)
                else -> return img

            }
        }
    }

    private fun rotateImage(img: Bitmap, degree: Float): Bitmap {

        val matrix = Matrix()
        matrix.postRotate(degree)
        val rotatedImg = Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
        return rotatedImg

    }
}