package com.sonin.soninandroidcommonlib.utils

import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.app.Activity
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale
import android.support.v4.app.Fragment
import com.sonin.soninandroidcommonlib.utils.PermissionManager.PermissionListener

class PermissionManager {

    interface PermissionListener{

        fun onPermissionAlreadyGranted()
        fun onExplainPermission()

    }

    companion object {

        /*
            Request permissions in the context of a Activity
        */
        fun requestPermissionsIfNecessary(activity: Activity, arrPermissions: Array<String>, listener: PermissionListener, intRequestCode: Int) {

            var blnPermissionAlreadyGranted = false

            for (i in arrPermissions.indices) {

                if (ContextCompat.checkSelfPermission(activity, arrPermissions[0]) != PackageManager.PERMISSION_GRANTED) {
                    // Should we show an explanation?
                    blnPermissionAlreadyGranted = false
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, arrPermissions[0])) {

                        listener.onExplainPermission()
                        ActivityCompat.requestPermissions(activity, arrPermissions, intRequestCode)

                    } else {

                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(activity, arrPermissions, intRequestCode)

                    }
                } else {

                    blnPermissionAlreadyGranted = true

                }

            }

            if (blnPermissionAlreadyGranted) {

                listener.onPermissionAlreadyGranted()

            }

        }


        /*
                Request permissions in the context of a fragment
         */
        fun requestPermissionsIfNecessary(fragment: Fragment, arrPermissions: Array<String>, listener: PermissionListener, intRequestCode: Int) {

            var blnPermissionAlreadyGranted = false

            for (i in arrPermissions.indices) {

                if (ContextCompat.checkSelfPermission(fragment.getActivity(), arrPermissions[i]) != PackageManager.PERMISSION_GRANTED) {

                    blnPermissionAlreadyGranted = false
                    // Should we show an explanation because the user has denied permission previously?
                    if (fragment.shouldShowRequestPermissionRationale(arrPermissions[i])) {

                        listener.onExplainPermission()
                        fragment.requestPermissions(arrPermissions, intRequestCode)


                    } else {

                        // No explanation needed, we can request the permission.
                        fragment.requestPermissions(arrPermissions, intRequestCode)
                    }
                } else {

                    blnPermissionAlreadyGranted = true

                }

            }

            if (blnPermissionAlreadyGranted) {

                listener.onPermissionAlreadyGranted()

            }

        }
    }


}
