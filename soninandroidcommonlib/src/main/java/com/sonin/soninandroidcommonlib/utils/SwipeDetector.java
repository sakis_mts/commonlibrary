package com.sonin.soninandroidcommonlib.utils;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class SwipeDetector implements View.OnTouchListener{

    private int         min_distance = 200;
    private float       downX, downY, upX, upY,moveX,moveY;
    private View        v;
    private ImageView  iv;
    public static final int   ON_MOVE_DETECTION    = 0;
    public static final int   ON_FINISH_DETECTION  = 1;
    private int        detection_mode;

    private onSwipeEvent swipeEventListener;

    public SwipeDetector(View v, int intMode){
        this.v=v;
        this.detection_mode = intMode;
        v.setOnTouchListener(this);
    }

    public void setOnSwipeListener(onSwipeEvent listener)
    {
        try{
            swipeEventListener=listener;
        }
        catch(ClassCastException e)
        {
            Log.e("ClassCastException","please pass SwipeDetector.onSwipeEvent Interface instance",e);
        }
    }


    public void onRightToLeftSwipe(){
        if(swipeEventListener!=null)
            swipeEventListener.SwipeEventDetected(v,SwipeTypeEnum.RIGHT_TO_LEFT);
        else
            Log.e("SwipeDetector error","please pass SwipeDetector.onSwipeEvent Interface instance");
    }

    public void onLeftToRightSwipe(){
        if(swipeEventListener!=null)
            swipeEventListener.SwipeEventDetected(v,SwipeTypeEnum.LEFT_TO_RIGHT);
        else
            Log.e("SwipeDetector error","please pass SwipeDetector.onSwipeEvent Interface instance");
    }

    public void onTopToBottomSwipe(){
        if(swipeEventListener!=null)
            swipeEventListener.SwipeEventDetected(v,SwipeTypeEnum.TOP_TO_BOTTOM);
        else
            Log.e("SwipeDetector error","please pass SwipeDetector.onSwipeEvent Interface instance");
    }

    public void onBottomToTopSwipe(){
        if(swipeEventListener!=null)
            swipeEventListener.SwipeEventDetected(v,SwipeTypeEnum.BOTTOM_TO_TOP);
        else
            Log.e("SwipeDetector error","please pass SwipeDetector.onSwipeEvent Interface instance");
    }

    public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_MOVE:
                moveX = event.getX();
                moveY = event.getY();

                float movedeltaX = downX - moveX;
                float movedeltaY = downY - moveY;

                //HORIZONTAL SCROLL
                if(Math.abs(movedeltaX) > Math.abs(movedeltaY))
                {

                    if(Math.abs(movedeltaX) > min_distance){
                        // left or right
                        if(movedeltaX < 0)
                        {
                            if (this.detection_mode == ON_MOVE_DETECTION){
                                this.onLeftToRightSwipe();
                            }

                            return true;
                        }
                        if(movedeltaX > 0) {

                            if (this.detection_mode == ON_MOVE_DETECTION){
                                this.onRightToLeftSwipe();
                            }

                            return true;
                        }
                    }
                    else {
                        //not long enough swipe...
                        return false;
                    }
                }
                break;
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                downY = event.getY();
                return true;
            }
            case MotionEvent.ACTION_UP: {
                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                //HORIZONTAL SCROLL
                if(Math.abs(deltaX) > Math.abs(deltaY))
                {
                    if(Math.abs(deltaX) > min_distance){
                        // left or right
                        if(deltaX < 0)
                        {
                            if (this.detection_mode == ON_FINISH_DETECTION){
                                this.onLeftToRightSwipe();
                            }

                            return true;
                        }
                        if(deltaX > 0) {
                            if (this.detection_mode == ON_FINISH_DETECTION){
                                this.onRightToLeftSwipe();
                            }

                            return true;
                        }
                    }
                    else {
                        //not long enough swipe...
                        return false;
                    }
                }
                //VERTICAL SCROLL
                else
                {
                    if(Math.abs(deltaY) > min_distance){
                        // top or down
                        if(deltaY < 0)
                        { this.onTopToBottomSwipe();
                            return true;
                        }
                        if(deltaY > 0)
                        { this.onBottomToTopSwipe();
                            return true;
                        }
                    }
                    else {
                        //not long enough swipe...
                        return false;
                    }
                }

                return true;
            }
        }
        return false;
    }
    public interface onSwipeEvent
    {
        public void SwipeEventDetected(View v, SwipeTypeEnum SwipeType);
    }

    public SwipeDetector setMinDistanceInPixels(int min_distance)
    {
        this.min_distance=min_distance;
        return this;
    }

    public enum SwipeTypeEnum
    {
        RIGHT_TO_LEFT,LEFT_TO_RIGHT,TOP_TO_BOTTOM,BOTTOM_TO_TOP
    }

}

