package com.sonin.soninandroidcommonlib.validation;

public class DefaultNotEmptyPolicy extends Policy {

	public DefaultNotEmptyPolicy() {

		this.blnValidateEmpty = true;
		this.blnValidateMin = false;
		this.blnValidateMax = false;

	}

}