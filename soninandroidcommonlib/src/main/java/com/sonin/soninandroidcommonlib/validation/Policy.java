package com.sonin.soninandroidcommonlib.validation;

public abstract class Policy {

	public interface CustomPolicyCallback {

		boolean onCustomPolicy(String strValidate);

	}

	protected boolean blnValidateEmpty;
	protected boolean blnValidateMin;
	protected boolean blnValidateMax;

	protected int intMaxLength;
	protected int intMinLength;

	private CustomPolicyCallback customPolicy;

	public boolean validatePolicy(Field field) {
		
		String strValidate = field.getString();
		if (blnValidateEmpty != false) {

			if (ValidationUtils.validateEmpty(strValidate) == false) {
				field.setStrFailure("This field cannot be empty");
				return false;
			}

		}

		if (blnValidateMin != false) {

			if (strValidate.length() < intMinLength) {
				field.setStrFailure("Field must be " + intMinLength + " characters");
				return false;
			}
		}

		if (blnValidateMax != false) {
			if (strValidate.length() > intMaxLength) {
				field.setStrFailure("Field must be less than " + intMaxLength + " characters");
				return false;
			}
		}

		if (customPolicy != null) {
			if (customPolicy.onCustomPolicy(strValidate) == false) {
				return false;
			}
		}

		return true;
	}

    public void setMaxLength(int maxLength) {
        this.intMaxLength = maxLength;
        this.blnValidateMax = true;
    }

    public void setMinLength(int minLength) {
        this.intMinLength = minLength;
        this.blnValidateMin = true;
    }

    public void setCustomPolicy(CustomPolicyCallback customPolicy) {
        this.customPolicy = customPolicy;
    }

}
